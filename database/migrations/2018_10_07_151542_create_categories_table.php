<?php

use App\Http\Controllers\AppConfig;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

    public function up() {

        Schema::create(AppConfig::TABLE_NAME_CATAGORY, function (Blueprint $table) {
            $table->increments('id');
            $table->string('cat_name');
            $table->timestamps();
        });
    }


    public function down() {

        Schema::dropIfExists('categories');
    }
}
