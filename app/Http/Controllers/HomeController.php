<?php

namespace App\Http\Controllers;

use App\Category;
use App\Discount;
use App\Product;
use App\SubCat;
use App\User;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

//        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $users = User::all();

        return view('home', compact('users'));
    }

    public function addCat() {

        return view('category');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userHome() {
        $id = Auth::id();
        Session::put('user_id', $id);
        $user = User::find($id);
        Session::put('user_name', $user['name']);

        $categories                     = Category::all();
        $sub_categories                 = SubCat::all();
        $discounted_products            = Discount::getAllDiscountedProducts();
        $all_products                   = Product::getAllProducts();

        return view('user/home', compact('categories', 'sub_categories', 'discounted_products', 'all_products'));
    }

    public function viewSingleProduct($id) {

        $product = Product::find($id);


        return view('user/single', compact('product'));
    }




}
