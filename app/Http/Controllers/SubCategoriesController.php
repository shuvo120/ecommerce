<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

Use App\SubCat;
Use App\Category;
use DataTables;

class subCategoriesController extends Controller
{
    public function index($id) {

        $values     = SubCat::getAllSubCategory($id);
        $category   = Category::getAllCategory($id);

        return view('admin/sub_category', compact('values', 'category'));
    }


    public function addCat(Request $request) {

        if($request->get('button_action') == 'insert') {

            $data = new SubCat([
                'sub_cat_name'          => $request->get('sub_cat_name'),
                'cat_id'                => $request->get('cat_id'),
            ]);
            $data->save();

            $success_output = 'Data Inserted';

            echo json_encode($success_output);
        }

        if($request->get('button_action') == 'update') {

            $data = SubCat::find($request->get('sub_cat_id'));
            $data ->sub_cat_name =    $request->get('sub_cat_name');
            $data->save();

            $success_output = '<div class="alert alert-success"> Data Updated</div>>';

            echo json_encode($success_output);
        }

    }

    public function deleteCat(Request $request) {

        $data = SubCat::find($request->input('id'));
        $data->delete();

        echo 'Data Deleted';
    }

    public function editCat(Request $request) {

        $data = SubCat::find($request->input('id'));
        $output = array(
            'sub_cat_name'         => $data->sub_cat_name
        );

        echo json_encode($output);

    }


}
