<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Category;
Use App\SubCat;
Use App\Product;
Use App\Discount;
Use DB;

class discountController extends Controller
{
    public function index() {

        $product_list =           Product::getAllProducts();
        $values       =           Discount::getAllDiscountedProducts();

        return view('admin\discount', compact('values','product_list'));
    }

    public function addProduct(Request $request) {

        if($request->get('button_action') == 'insert') {

            $data = new Discount([
                'pro_id'                 => $request->get('product_id'),
                'pro_discount'           => $request->get('pro_discount')
            ]);

            $data->save();
            $success_output = 'Data Inserted';

            echo json_encode($success_output);
        }

    }
    public function updateProduct(Request $request) {

        $data = Discount::find($request->get('dis_pro_id'));
        $data ->pro_discount =    $request->get('product_discount');
        $data->save();

        $success_output = '<div class="alert alert-success"> Data Updated</div>>';


        echo json_encode($success_output);
    }

    public function editProduct(Request $request) {

        $data = Discount::find($request->input('id'));
        $output = array(
            'pro_discount'                => $data->pro_discount,
        );

        echo json_encode($output);

    }

    public function deleteProduct(Request $request) {

        $data = Discount::find($request->input('id'));
        $data->delete();
        echo "Data Deleted";
    }
}
