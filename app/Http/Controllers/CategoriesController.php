<?php

namespace App\Http\Controllers;

use App\Category;

use Illuminate\Http\Request;


class CategoriesController extends Controller {

    public function index() {

        return view('admin/category');
    }


    public function getAllCat() {

        $values = Category::all();

        return view('admin/category', compact('values'));
    }

    /**
     * @param Request $request
     */
    public function addCat(Request $request) {

        if($request->get('button_action') == 'insert') {

            $data = new Category([
                'cat_name'          => $request->get('cat_name')
            ]);

            $data->save();
            $success_output = 'Data Inserted';

            echo json_encode($success_output);
        }

        if($request->get('button_action') == 'update') {

            $data = Category::find($request->get('cat_id'));
            $data ->cat_name =    $request->get('cat_name');
            $data->save();

            $success_output = '<div class="alert alert-success"> Data Updated</div>>';


            echo json_encode($success_output);
        }

    }

    public function deleteCat(Request $request) {

        $data = Category::find($request->input('id'));
        $data->delete();

        echo 'Data Deleted';
    }

    public function editCat(Request $request) {

        $data = Category::find($request->input('id'));

        $output = array(
            'cat_name'         => $data->cat_name
        );

        echo json_encode($output);
    }

    public function khan($id) {

        return $id;
    }
}
