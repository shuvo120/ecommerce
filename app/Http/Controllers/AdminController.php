<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller {

    public function index() {

        return view('admin/settings');
    }

    public function aboutUs() {

        return view('admin/about');
    }

    public function viewLogin() {

        return view('user/login');
    }

    public function viewRegistration() {

        return view('user/register');
    }
}
