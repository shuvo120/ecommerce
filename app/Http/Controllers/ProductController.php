<?php

namespace App\Http\Controllers;

use App\Discount;
use Cart;
use Illuminate\Http\Request;

Use App\Category;
Use App\SubCat;
Use App\Product;
use Illuminate\Support\Facades\Storage;
use Image;
use DB;
Use Validator;

class productController extends Controller
{
    public function index() {

        $category_list = Category::all();

        return view('admin/post', compact('category_list'));
    }


    public function showProducts() {

        $values = Product::getAllProductswithoutPagination();
//        $values = Product::all();
        return view('admin/view_products', compact('values'));
    }


    public function getSubCat(Request $request) {

        $select = $request->get('select');
        $id = $request->get('id');
        $dependent = $request->get('dependent');

        $data = DB::table('sub_cats')
            ->where('cat_id', $id)
            ->get();

//        $output = '<option value="">'.ucfirst($dependent).'</option>';
        $output = '<option value="" hidden> Select Sub Category </option>';

        foreach ($data as $row) {

            $output .= '<option value="'.$row->id.'">'.$row->sub_cat_name.'</option>';
        }

        echo $output;
    }

    public function uploadPost(Request $request) {

        $validation = Validator::make($request->all(),[
           'post_image' =>  'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        if($validation->passes()) {

            $image = $request->file('pro_image');
            $new_name = rand().'.'.$image->getClientOriginalExtension();
            $image->move(public_path('/images/post_images'),$new_name);

            $data = new Product([
                'pro_name'                  => $request->get('pro_name'),
                'pro_price'                 => $request->get('pro_price'),
                'pro_description'           => $request->get('pro_description'),
                'subCat_id'                 => $request->get('sub_cat'),
                'pro_cnt'                   => $request->get('pro_cnt'),
                'pro_img'                   => $new_name,
            ]);
            $data->save();

            return response()->json([
                'message' => 'Image upload successfully',
                'after_image' => '',
                'class_name' => 'alert-success',
            ]);
        }
        else
        {
            return response()->json([
                'message' => $validation->errors->all(),
                'after_image' => '',
                'class_name' => 'alert',
            ]);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function uploadTest(Request $request) {

        $this->validate($request,[
            'pro_image' =>  'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $image = $request->file('pro_image');
        $new_name = rand().'.'.$image->getClientOriginalExtension();

        Storage::put('public/post_images/'. $new_name, fopen($image, 'r+'));
        Storage::put('public/post_images/thumbnail/'. $new_name, fopen($image, 'r+'));

//        $image->move(public_path('/images/post_images'),$new_name);

        $thumbnailpath = public_path('storage/post_images/thumbnail/'.$new_name);
        $img = Image::make($thumbnailpath)->resize(350, 438, function($constraint) {
            $constraint->aspectRatio();
        });

        $img->save($thumbnailpath);

        $data = new Product([
            'pro_name'                  => $request->get('pro_name'),
            'pro_price'                 => $request->get('pro_price'),
            'pro_description'           => $request->get('pro_description'),
            'subCat_id'                 => $request->get('subCat_id'),
            'pro_cnt'                   => $request->get('pro_cnt'),
            'pro_img'                   => $new_name,
        ]);
        $data->save();

        return redirect()->back()->with('message', 'Form Upload Successfully');

    }

    public function deleteProduct(Request $request) {

        $data = Product::find($request->input('id'));
        $data->delete();
        echo "Data Deleted";
    }

    public function editProduct(Request $request) {

        $data = Product::find($request->input('id'));
        $output = array(
            'pro_name'                => $data->pro_name,
            'pro_price'               => $data->pro_price,
            'pro_description'         => $data->pro_description,
            'pro_cnt'                 => $data->pro_cnt,
        );

        echo json_encode($output);

    }

    public function updateProduct(Request $request) {

        $data = Product::find($request->get('pro_id'));
        $data ->pro_name            =    $request->get('pro_name');
        $data ->pro_price           =    $request->get('pro_price');
        $data ->pro_description     =    $request->get('pro_description');
        $data ->pro_cnt             =    $request->get('pro_cnt');
        $data->save();

        $success_output = '<div class="alert alert-success"> Data Updated</div>>';


        echo json_encode($success_output);
    }

    public function getProductAccordingToSubcat($name, $id) {

        $categories                     = Category::all();
        $sub_categories                 = SubCat::all();
        $discounted_products            = Discount::getAllDiscountedProducts();
        $all_products                   = Product::getProductAccordingToSubcat($id);
        $cartCollection                 = Cart::getTotalQuantity();

        return view('user/home', compact('categories', 'sub_categories', 'discounted_products', 'all_products', 'cartCollection'));

    }


}
