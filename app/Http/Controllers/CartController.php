<?php

namespace App\Http\Controllers;

//use Darryldecode\Cart\Cart;


use App\Product;
use Illuminate\Http\Request;

use Cart;

class CartController extends Controller {

    public function index() {

        $userId = auth()->user()->id;

        return Cart:: getContent();
    }

    public function addCart(Request $request) {

//        $userId = auth()->user()->id;
//        Cart::session($userId)->add(455, 'Sample Item', 100.99, 2, array());
        $product = Product::find($request->get('pro_id'));
//        return $product;

//        Cart::session($userId)->add([
//            'id' => $product -> id,
//            'name' => $product -> pro_name,
//            'price' => $product -> pro_price,
//            'quantity' => $request->get('pro_quantity'),
//            'attributes' => array(
//                'image' => $product -> pro_img,
//            )
//        ]);

        Cart:: add([
            'id' => $product -> id,
            'name' => $product -> pro_name,
            'price' => $product -> pro_price,
            'quantity' => $request->get('pro_quantity'),
            'attributes' => array(
                'image' => $product -> pro_img,
            )
        ]);
//        Cart::getContent();

        return "okay";
//        Cart::add(455, 'Sample Item', 100.99, 2, array());
    }

    public function checkCart() {

        $userId = auth()->user()->id;
//        $carts = Cart::session($userId)->getContent();
        $carts = Cart::getContent();
//        return $carts;
        return view('user/checkout', compact('carts'));
    }

    public function getSingleProduct(Request $request) {

        $data = Product::find($request->input('id'));

        $output = array(
            'pro_name'                => $data->pro_name,
            'pro_price'               => $data->pro_price,
            'pro_description'         => $data->pro_description,
            'pro_cnt'                 => $data->pro_cnt,
            'pro_image'               => $data->pro_img,
        );

        echo json_encode($output);
    }

    public function removeSingleProduct(Request $request) {

        $userId = auth()->user()->id;

        Cart:: remove($request->input('id'));

        return "okay";
    }
}