<?php

namespace App\Http\Controllers;

use App\ProductsReview;
use Illuminate\Http\Request;

class ProductsReviewController extends Controller
{


    public function addReview(Request $request) {

//        return $request;

        $data = new ProductsReview([

            'user_id'           =>  $request->get('user_id'),
            'pro_id'            =>  $request->get('pro_id'),
            'pro_review'        =>  $request->get('pro_review'),
        ]);

        $data->save();

//        return "ok";
        return redirect()->back()->with('message', 'Form Upload Successfully');

    }
}
