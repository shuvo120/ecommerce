<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppConfig
{
    const BLANK                                                          =   "";
    const ROUTE_DELETE                                                   =   "delete.of.user";
    const SETTINGS                                                       =   "settings";
    const SHOW_ALL_CATEGORY                                              =   "index";
    const ADD_CATEGORY                                                   =   "post.cat";
    const ADD_SUB_CATEGORY                                               =   "post.sub.cat";
    const ADD_NEW_PRODUCT                                                =   "upload.test";
    const ABOUT_US                                                       =   "about.us";
    const SHOW_ALL_DISCOUNTED_PRODUCTS                                   =   "discount";
    const SHOW_SINGLE_PRODUCT                                            =   "single.product";
    const USER_LOGIN                                                     =   "user.login";
    const USER_REGISTRATION                                              =   "user.registration";
    const USER_HOME_PAGE                                                 =   "user.home";
    const GET_PRODUCT_ACCORDING_TO_SUBCATEGORY                           =   "pro.ac.sub";
    const ADD_CART                                                       =   "add.cart";
    const CART_CHECKOUT                                                  =   "checkout.cart";
    const ADD_A_REVIEW                                                   =   "add.a.review";




    // all ajax call found here
    const ROUTE_SETTINGS_AJX                                              =   "settings.ajx";
    const ROUTE_DELETE_CATEGORY_AJAX                                      =   "delete.cat";
    const EDIT_CATEGORY_AJAX                                              =   "edit.cat";
    const DELETE_SUB_CATEGORY_AJAX                                        =   "delete.sub_cat";
    const EDIT_SUB_CATEGORY_AJAX                                          =   "edit.sub_cat";
    const SUBCATEGORY_ACCORDING_TO_CATEGORY_AJAX                          =   "get_sub_cat";
    const ADD_NEW_PRODUCT_AJAX                                            =   "upload.post";
    const EDIT_PRODUCT_AJAX                                               =   "edit.product";
    const DELETE_PRODUCT_AJAX                                             =   "delete.product";
    const UPDATE_PRODUCT_AJAX                                             =   "update.product";
    const ADD_NEW_DISCOUNTED_PRODUCT_AJAX                                 =   "discount.product";
    const EDIT_DISCOUNTED_PRODUCT_AJAX                                    =   "discount.edit.product";
    const UPDATE_DISCOUNTED_PRODUCT_AJAX                                  =   "discount.update.product";
    const DELETE_DISCOUNTED_PRODUCT_AJAX                                  =   "discount.delete.product";
    const GET_SINGLE_PRODUCT_AJAX                                         =   "get.single.product";
    const REMOVE_SINGLE_CART_AJAX                                         =   "remove.single.cart";


    //table Names
    const TABLE_NAME_CATEGORY                                            =   "categories";
    const TABLE_NAME_SUB_CATEGORY                                        =   "sub_cats";

}
