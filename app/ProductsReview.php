<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsReview extends Model
{
    protected $fillable = ['user_id', 'pro_id', 'pro_review'];


}
