<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable  = ['pro_id', 'pro_discount'];

    public static function getAllDiscountedProducts(){

        return self::select('discounts.*', 'products.pro_name', 'products.pro_img', 'products.pro_price','sub_cats.sub_cat_name', 'categories.cat_name')
            -> join('products', 'discounts.pro_id','=', 'products.id')
            -> join('sub_cats', 'products.subCat_id','=', 'sub_cats.id')
            -> join('categories', 'sub_cats.cat_id','=', 'categories.id')
            -> get();
    }
}
