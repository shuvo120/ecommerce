<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCat extends Model
{
    protected $fillable  = ['sub_cat_name', 'cat_id'];

    public static function getAllSubCategory($id) {

        return self::select('Sub_cats.*')
            ->where('cat_id', $id)
            ->get();
    }
}
