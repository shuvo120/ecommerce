<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable  = ['cat_name'];

    public static function getAllCategory($id) {

        return self::select('Categories.*')
            ->where('id', $id)
            ->get()
            ->first();
    }
}
