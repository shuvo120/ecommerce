<?php

namespace App;

use App\Http\Controllers\AppConfig;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['pro_name','pro_price', 'pro_description', 'subCat_id', 'pro_cnt', 'pro_img'];

    public static function getAllProducts() {

        return self::select('products.*', AppConfig::TABLE_NAME_SUB_CATEGORY.'.sub_cat_name', AppConfig::TABLE_NAME_CATEGORY.'.cat_name')
            -> join(AppConfig::TABLE_NAME_SUB_CATEGORY, 'products.subCat_id','=', AppConfig::TABLE_NAME_SUB_CATEGORY.'.id')
            -> join('categories', 'sub_cats.cat_id','=', 'categories.id')
            ->paginate(9);
    }

    public static function getAllProductswithoutPagination() {

        return self::select('products.*', AppConfig::TABLE_NAME_SUB_CATEGORY.'.sub_cat_name', AppConfig::TABLE_NAME_CATEGORY.'.cat_name')
            -> join(AppConfig::TABLE_NAME_SUB_CATEGORY, 'products.subCat_id','=', AppConfig::TABLE_NAME_SUB_CATEGORY.'.id')
            -> join('categories', 'sub_cats.cat_id','=', 'categories.id')
            ->get();
    }

    public static function getProductAccordingToSubcat($id){
        return self::select('products.*')
            -> where('products.subCat_id', $id)
            ->paginate(9);
    }
}
