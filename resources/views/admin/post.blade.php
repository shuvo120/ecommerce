@extends('adminlte::page')

@section('title', 'Create post')

@section('content_header')
    <h1></h1>
@stop

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Add a product</h3>

        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">

                @if(count($errors) >0)
                <div class="alert alert-danger" id="message">
                    Upload valiead image <br>

                    <ul>
                        @foreach($errors->all() as $error)
                            <li> {{$error}}</li>
                        @endforeach

                    </ul>

                </div>
                @endif

            @if(session()->has('message'))
                {{--<strong> {{$message}}</strong>}}--}}
                        <div class="alert alert-success">

                            {{session()->get('message')}}
                        </div>
                @endif


            <form method="post" id="post_form" action="{{route(\App\Http\Controllers\AppConfig::ADD_NEW_PRODUCT)}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <br>

                <div class="row fix">
                    <div class="form-group col-md-2">
                        <select name="category" id="category" class="from-control input-lg dynamic" data-dependent="subCat_id">
                            <option value="" hidden> Select Category </option>

                            @foreach($category_list as $category)
                                <option value="{{$category->id}}"> {{$category->cat_name}}</option>
                            @endforeach

                        </select>

                    </div>
                </div>

                <div class="row fix">
                    <div class="form-group col-md-2">
                        <select name="subCat_id" id="subCat_id" class="from-control input-lg">
                            <option value=""> Select Sub Category </option>
                        </select>

                    </div>

                </div>

                <div class="row fix">
                    <div class="form-group mb-3 col-md-2">
                        <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                        <div class="input-group">
                            <div class="input-group-addon">name: </div>
                            <input type="text" class="form-control" name="pro_name" id="pro_name" placeholder="Product name">
                        </div>
                    </div>

                </div>

                <div class="row fix">
                    <div class="form-group mb-3 col-md-2">
                        <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="number" class="form-control" name="pro_price" id="pro_price" placeholder="Amount">
                            <div class="input-group-addon">.00</div>
                        </div>
                    </div>

                </div>

                <div class="row fix">
                    <div class="form-group mb-3 col-md-2 d-block">
                        <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="number" class="form-control" name="pro_cnt" id="pro_cnt" placeholder="Quantity">
                            <div class="input-group-addon">.00</div>
                        </div>
                    </div>

                </div>

                <div class="row fix">
                    <div class="col-md-2 d-block">
                        <textarea class="form-control " rows="3" placeholder="Description" name="pro_description" id="pro_description"></textarea>
                    </div>
                </div>
                <br>

                <div class="row fix">
                    <div class="col-md-2 d-block">
                        <input type="file" class="form-control" name="pro_image" id="pro_image">
                    </div>
                </div>
                <br>


                <div class="row fix">
                    <div class="col-md-2 mt-md-5">
                        {{--<button class="btn-success">Submit</button>--}}
                        <input type="submit" name="upload" class="bt btn-primary" value="upload">
                    </div>
                </div>

                <br>


            </form>
        </div>
        <!-- /.box-body -->
    </div>
@stop

@section('js')

    <script>
        $(document).ready(function(){
            $('.dynamic').change(function () {
                if($(this).val() != '') {
                    var select = $(this).attr("id");
                    var id = $(this).val();
                    var dependent = $(this).data('dependent');

                    console.log(id);

                    $.ajax({
                        url: "{{route(\App\Http\Controllers\AppConfig::SUBCATEGORY_ACCORDING_TO_CATEGORY_AJAX)}}",
                        method: "post",
                        data:
                            {
                                select: select,
                                id: id,
                                _token: "{{csrf_token()}}",
                                dependent: dependent,

                            },
                        success: function (result) {
                            $('#'+dependent).html(result);
                        }
                    })
                }
            });
        });

        {{--$(document).ready(function () {--}}
            {{--$('#post_form').on('submit', function (event) {--}}
                {{--event.preventDefault();--}}
                {{--// var form_data = $(this).serialize();--}}

                {{--$.ajax({--}}
                    {{--url:"{{route('upload.post')}}",--}}
                    {{--method: "POST",--}}
                    {{--data: new FormData(this),--}}
                    {{--dataType: 'JSON',--}}
                    {{--contentType: false,--}}
                    {{--cache: false,--}}
                    {{--processData: false,--}}

                    {{--success:function(data)--}}
                    {{--{--}}
                        {{--location.reload();--}}
                        {{--// $('#message').css('display', 'block');--}}
                        {{--// $('#message').html(data.message);--}}
                        {{--// $('#message').addClass(data.class_name);--}}
                        {{--// $('#uploaded_post').html(data.uploaded_post);--}}
                        {{--console.log("thik ase");--}}
                    {{--},--}}
                    {{--error:function (error) {--}}
                        {{--console.log(error);--}}

                    {{--},--}}
                {{--})--}}

            {{--});--}}

        {{--});--}}

        {{--$('#post_form').on('submit', function(event){--}}
            {{--event.preventDefault();--}}
            {{--var form_data = $(this).serialize();--}}
            {{--$.ajax({--}}
                {{--url:'{{route('upload.post')}}',--}}
                {{--type:"post",--}}
                {{--data:form_data,--}}
                {{--dataType:"json",--}}

                {{--success:function(data)--}}
                {{--{--}}
                    {{--console.log(data);--}}

                {{--}--}}
            {{--})--}}
        {{--});--}}


    </script>
@stop