@extends('adminlte::page')

{{--@extends('layouts.admin_layout')--}}


@section('title', 'Sub Category')

@section('content_header')
    {{--<h1 align="center"> All Categories</h1>--}}
@stop

@section('content')

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">All Categories  <i class="glyphicon glyphicon-chevron-right"></i> {{$category->cat_name}}</h3>

            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <button type="button" name="add" id="add_data" class="glyphicon glyphicon-plus btn btn-success btn-lg pull-right">Add</button>

                    <br><br>
                    <br><br>

                    <div class="modal fade" id="catModal" role="dialog">
                        <div class="modal-dialog">

                            <div class="modal-content">

                                <form id="cat_form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Insert Category Name</h4>
                                    </div>

                                    {{csrf_field()}}
                                    <div class="modal-body">
                                        <span id="form_output"></span>
                                        <div class="content">
                                            <label for="sub_cat_name"><b>Category name: </b></label>
                                            <br>
                                            <input type="text" placeholder="put a nice category name" name="sub_cat_name" id="sub_cat_name" class="form-control" required>
                                            <input type="hidden" name="cat_id" id="cat_id" value="{{$category->id}}" />
                                            <br><br>

                                        </div>

                                        <div class="modal-footer">
                                            <input type="hidden" name="sub_cat_id" id="sub_cat_id" value="" />
                                            <input type="hidden" name="button_action" id="button_action" value="insert" />
                                            <input type="submit" name="submit" id="action" value="Add" class="btn btn-info" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>


                    <table id="cat_table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($values as $value)
                            <tr>
                                <td>{{$value->sub_cat_name}}</td>
                                <td>{{$value->created_at}}</td>
                                <td><button class = "edit glyphicon glyphicon-edit btn btn-primary a-btn-slide-text" token="{{csrf_token()}}" id="{{$value->id}}">Edit</button>
                                    <button class = "delete glyphicon glyphicon-remove btn btn-primary a-btn-slide-text" token="{{csrf_token()}}" id="{{$value->id}}">Delete</button>
                                    {{--<a href="{{url('subCat/'.$value->id)}}"> <i class="fa fa-arrow-right pull-right" style="font-size:30px;color:red"></i> </a>--}}

                                </td>
                            </tr>
                        @endforeach
                        </tbody>


                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

            <div class="box-footer clearfix">
                {{--<a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>--}}
                {{--<a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>--}}
            </div>
            <!-- /.box-footer -->
        </div>




        @stop

@section('js')


    <script type="text/javascript">


        $(document).ready(function() {
            $('#cat_table').DataTable(

            );
        } );

        $('#add_data').click(function () {
            $('#catModal').modal('show');
            $('#catForm')[0].reset();
            $('#form_output').html('Yes');
            $('#button_action').val('insert');
            $('#action').val('Add');
        });

        $('#cat_form').on('submit', function(event){
            event.preventDefault();
            var form_data = $(this).serialize();
            // console.log('SHuvo');
            $.ajax({
                url:'{{route(\App\Http\Controllers\AppConfig::ADD_SUB_CATEGORY)}}',
                type:"post",
                data:form_data,
                dataType:"json",

                success:function(data)
                {
                    console.log(data);
                    $('#cat_form')[0].reset();
                    $('#action').val('Add');
                    $('.modal-tittle').text('Add Data');
                    $('#button_action').val('insert');
                    $('#result').html(data);
                    $('#result').addClass('alert alert-success');
                    location.reload();

                }
            })
        });

        $(document).on('click', '.delete', function(){
            var id = $(this).attr('id');
            if(confirm("Are you sure?"))
            {
                $.ajax({
                    beforeSend: function(){

                    },

                    type: 'POST',
                    url:'{{ route(\App\Http\Controllers\AppConfig::DELETE_SUB_CATEGORY_AJAX) }}',
                    data: {
                        "_token": "{{csrf_token()}}",
                        id:id
                    },
                    cache: false,
                    dataType: 'HTML',

                    success: function(data){
                        console.log(data);
                        $('#result').html(data);
                        $('#result').addClass('alert alert-info alert-dismissible fade in');

                        // remove tr after successfully deleted data and the should be a successful or error message
                        location.reload();
                    }
                });

            }
            else
            {
                return false;
            }
        });

        $(document).on('click', '.edit', function(){
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '{{route(\App\Http\Controllers\AppConfig::EDIT_SUB_CATEGORY_AJAX)}}',
                data: {
                    "_token" : '{{csrf_token()}}',
                    id:id
                },
                dataType: 'json',

                success:function(data){
                    // console.log(id);
                    // console.log(data);

                    $('#sub_cat_name').val(data.sub_cat_name);
                    $('#catModal').modal('show');
                    $('#action').val('Done');
                    $('#sub_cat_id').val(id);
                    $('.modal-title').text('Edit Data');
                    $('#button_action').val('update');
                }
            });

        });

    </script>
@stop

