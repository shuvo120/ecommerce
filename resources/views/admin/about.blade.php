@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')

    <div style="padding: 20px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;"><h3 href="https://themequarry.com" style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">We don't have any qualification. but we are good human being :) !</h3></div>

@stop

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"> </h3>

        </div>

        <!-- /.box-header -->
        <div class="box-body">



            <br>
            <br>

            <div class="row">


                <div class="col-lg-4 text-center">
                    <img src="{{asset('images\profile_images\junaid_vai.png')}}" class="img-circle" alt="Cinque Terre" width="304" height="304">

                </div>

                <div class="col-lg-4 text-center">
                    <img src="{{asset('images\profile_images\tonmoy_vai.jpg')}}" class="img-circle" alt="Cinque Terre" width="304" height="304">

                </div>

                <div class="col-lg-4 text-center">
                    <img src="{{asset('images\profile_images\me.jpg')}}" class="img-circle" alt="Cinque Terre" width="304" height="304">

                </div>

            </div>


            <div class="row">

                <div class="col-lg-4 text-center">
                    <h3> Junaid Ahmed</h3>

                </div>

                <div class="col-lg-4 text-center">
                    <h3> Tonmoy Deb Chowdhury</h3>

                </div>

                <div class="col-lg-4 text-center">
                    <h3> Foysol Ahmed Shuvo</h3>

                </div>

            </div>

            <div class="row">

                <div class="col-lg-4 text-center">
                    <h3> <strong> Owner</strong></h3>

                </div>

                <div class="col-lg-4 text-center">
                    <h3> <strong> Moderator</strong></h3>

                </div>

                <div class="col-lg-4 text-center">
                    <h3> <strong> Developer</strong> </h3>

                </div>

            </div>
        </div>
    </div>
@stop

@section('js')
@stop
