@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1></h1>
@stop

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">All Products </h3>

        </div>

        <!-- /.box-header -->
        <div class="box-body">

            <div class="modal fade" id="productModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <form id="product_form">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Insert Category Name</h4>
                            </div>

                            {{csrf_field()}}
                            <div class="modal-body">
                                <span id="form_output"></span>
                                <div class="content">
                                    <label for="pro_name"><b>Product name: </b></label>
                                    <br>
                                    <input type="text" name="pro_name" id="pro_name" class="form-control" required>
                                    <br>

                                    <label for="pro_price"><b>Product price: </b></label>
                                    <br>
                                    <input type="number" name="pro_price" id="pro_price" class="form-control" required>
                                    <br>

                                    <label for="pro_description"><b>Product description: </b></label>
                                    <br>
                                    <input type="text" name="pro_description" id="pro_description" class="form-control" required>
                                    <br>

                                    <label for="pro_cnt"><b>Quantity: </b></label>
                                    <br>
                                    <input type="number" name="pro_cnt" id="pro_cnt" class="form-control" required>
                                    <br>

                                </div>

                                <div class="modal-footer">
                                    <input type="hidden" name="pro_id" id="pro_id" value="" />
                                    <input type="hidden" name="button_action" id="button_action" value="insert" />
                                    <input type="submit" name="submit" id="action" value="Add" class="btn btn-info" />
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </form>


                    </div>

                </div>
            </div>



            <div>
                <table id="products_table"  class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Product Price</th>
                            <th>Product Description</th>
                            <th>Quantity</th>
                            <th>Created at</th>
                            <th>Image</th>
                            <th>actions</th>
                        </tr>
                    </thead>

                    <tbody id="result">

                    @foreach($values as $value)
                        <tr>
                            <td>{{$value->pro_name}}</td>
                            <td>{{$value->cat_name}}</td>
                            <td>{{$value->sub_cat_name}}</td>
                            <td>{{$value->pro_price}}</td>
                            <td>{{$value->pro_description}}</td>
                            <td>{{$value->pro_cnt}}</td>
                            <td>{{$value->created_at}}</td>
                            <td><img src="{{url('storage/post_images/thumbnail/'.$value->pro_img)}}" style="width:90px;height:90px;"></td>

                            <td><button class = "edit glyphicon glyphicon-edit btn btn-primary a-btn-slide-text" token="{{csrf_token()}}" id="{{$value->id}}">Edit</button>
                                <button class = "delete glyphicon glyphicon-remove btn btn-primary a-btn-slide-text" token="{{csrf_token()}}" id="{{$value->id}}">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                    <tfoot>
                        <tr>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Product Price</th>
                            <th>Product Description</th>
                            <th>Quantity</th>
                            <th>Created at</th>
                            {{--<th>Image</th>--}}
                            {{--<th>actions</th>--}}
                        </tr>

                    </tfoot>


                </table>
            </div>
        </div>
    </div>

@stop

@section('js')
    <script type="text/javascript">

        $(document).ready(function() {
            $('#products_table').DataTable(
                {
                    columnDefs: [
                        { width: 100, targets: 0 }
                    ],
                },

            );
        } );

        // $(document).ready(function() {
        //     var table = $('#products_table').removeAttr('width').DataTable( {
        //         scrollY:        "300px",
        //         scrollX:        true,
        //         scrollCollapse: true,
        //         paging:         false,
        //         columnDefs: [
        //             { width: 200, targets: 0 }
        //         ],
        //         fixedColumns: true
        //     } );
        // } );



        $(document).on('click', '.delete', function(){
            var id = $(this).attr('id');
            if(confirm("Are you sure?"))
            {
                $.ajax({
                    beforeSend: function(){

                    },

                    type: 'POST',
                    url:'{{ route(\App\Http\Controllers\AppConfig::DELETE_PRODUCT_AJAX) }}',
                    data: {
                        "_token": "{{csrf_token()}}",
                        id:id
                    },
                    cache: false,
                    dataType: 'HTML',

                    success: function(data){
                        console.log(data);
                        // $('#result').html(data);
                        // $('#result').addClass('alert alert-info alert-dismissible fade in');

                        // remove tr after successfully deleted data and the should be a successful or error message
                        location.reload();
                    }
                });

            }
            else
            {
                return false;
            }
        });

        $(document).on('click', '.edit', function(){
            var id = $(this).attr('id');
            $.ajax({
                type: 'post',
                url: '{{route(\App\Http\Controllers\AppConfig::EDIT_PRODUCT_AJAX)}}',
                data: {
                    "_token" : '{{csrf_token()}}',
                    id:id
                },
                dataType: 'json',

                success:function(data){
                    console.log(id);
                    console.log(data);

                    $('#pro_name').val(data.pro_name);
                    $('#pro_price').val(data.pro_price);
                    $('#pro_description').val(data.pro_description);
                    $('#pro_cnt').val(data.pro_cnt);
                    $('#productModal').modal('show');
                    $('#action').val('Edit');
                    $('#pro_id').val(id);
                    $('.modal-title').text('Edit Data');
                    $('#button_action').val('update');
                }
            });

        });

        $('#product_form').on('submit', function(event){
            event.preventDefault();
            var form_data = $(this).serialize();
            $.ajax({
                url:'{{route(\App\Http\Controllers\AppConfig::UPDATE_PRODUCT_AJAX)}}',
                type:"post",
                data:form_data,
                dataType:"json",

                success:function(data)
                {
                    $('#product_form')[0].reset();

                    location.reload();

                }
            })
        });


        // $(document).ready(function() {
        //     $('#products_table').DataTable( {
        //         initComplete: function () {
        //             this.api().columns().every( function () {
        //                 var column = this;
        //                 var select = $('<select><option value=""></option></select>')
        //                     .appendTo( $(column.footer()).empty() )
        //                     .on( 'change', function () {
        //                         var val = $.fn.dataTable.util.escapeRegex(
        //                             $(this).val()
        //                         );
        //
        //                         column
        //                             .search( val ? '^'+val+'$' : '', true, false )
        //                             .draw();
        //                     } );
        //
        //                 column.data().unique().sort().each( function ( d, j ) {
        //                     select.append( '<option value="'+d+'">'+d+'</option>' )
        //                 } );
        //             } );
        //         }
        //     } );
        // } );

        $(document).ready(function() {
            var table = $('#products_table').DataTable();

            $('#products_table tbody')
                .on( 'mouseenter', 'td', function () {
                    var colIdx = table.cell(this).index().column;

                    $( table.cells().nodes() ).removeClass( 'highlight' );
                    $( table.column( colIdx ).nodes() ).addClass( 'highlight' );
                } );
        } );


    </script>
@stop
