@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1></h1>
@stop

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">All Discounted Products </h3>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <button type="button" name="add" id="add_data" class="glyphicon glyphicon-plus btn btn-success btn-lg pull-right">Add</button>

            <br><br>
            <br><br>

            <div class="modal fade" id="productModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <form id="product_form">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Insert a discounted product</h4>
                            </div>

                            {{csrf_field()}}
                            <div class="modal-body">
                                <span id="form_output"></span>
                                <div class="content">

                                    <select name="product_id" id="product_id" class="from-control input-lg">
                                        <option value="" hidden> Select Product Name </option>

                                        @foreach($product_list as $product)
                                            <option value="{{$product->id}}"> {{$product->pro_name}}</option>
                                        @endforeach

                                    </select>

                                    <br>
                                    <br>
                                    <br>
                                    <select name="pro_discount" id="pro_discount" class="from-control input-lg">
                                        <option value="" hidden> Add Discount</option>

                                        @for($i=1; $i <= 100; $i++)
                                            <option value="{{$i}}"> {{$i}} %</option>
                                        @endfor

                                    </select>
                                    <br>

                                </div>

                                <div class="modal-footer">
                                    <input type="hidden" name="pro_id" id="pro_id" value="" />
                                    <input type="hidden" name="button_action" id="button_action" value="insert" />
                                    <input type="submit" name="submit" id="action" value="Add" class="btn btn-info" />
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>

                        </form>


                    </div>

                </div>
            </div>

            <div class="modal fade" id="discountModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <form id="discount_form">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Insert a discounted product</h4>
                            </div>

                            {{csrf_field()}}
                            <div class="modal-body">
                                <span id="form_output"></span>
                                <div class="content">

                                    <br>
                                    <select name="product_discount" id="product_discount" class="from-control input-lg">
                                        <option value="" > Add Discount</option>

                                        @for($i=1; $i <= 100; $i++)
                                            <option value="{{$i}}"> {{$i}} %</option>
                                        @endfor

                                    </select>
                                    <br>

                                </div>

                                <div class="modal-footer">
                                    <input type="hidden" name="dis_pro_id" id="dis_pro_id" value="" />
                                    <input type="hidden" name="dis_button_action" id="dis_button_action" value="insert" />
                                    <input type="submit" name="submit" id="dis_action" value="Edit" class="btn btn-info" />
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </form>


                    </div>

                </div>
            </div>




            <div>
                <table id="products_table" class="table table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Product Price</th>
                        <th>After Discount</th>
                        <th>Discount</th>
                        <th>Created at</th>
                        <th>actions</th>
                    </tr>
                    </thead>

                    <tbody id="result">

                    @foreach($values as $value)
                        <tr>
                            <td>{{$value->pro_name}}</td>
                            <td>{{$value->cat_name}}</td>
                            <td>{{$value->sub_cat_name}}</td>
                            <td>{{$value->pro_price}}</td>
                            <td>{{floor(($value->pro_price) - (($value->pro_discount * $value->pro_price)/(100))) }}</td>
                            <td>{{$value->pro_discount}}</td>
                            <td>{{$value->created_at}}</td>
                            <td><button class = "edit glyphicon glyphicon-edit btn btn-primary a-btn-slide-text" token="{{csrf_token()}}" id="{{$value->id}}">Edit</button>
                                <button class = "delete glyphicon glyphicon-remove btn btn-primary a-btn-slide-text" token="{{csrf_token()}}" id="{{$value->id}}">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                    <tfoot>
                        <tr>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Product Price</th>
                            <th>After Discount</th>
                            <th>Discount</th>
                        </tr>

                    </tfoot>


                </table>
            </div>
        </div>
    </div>

@stop

@section('js')
    <script type="text/javascript">

        // $(document).ready(function() {
        //     $('#products_table').DataTable(
        //
        //     );
        // } );


        $('#product_id').select2();
        $('#pro_discount').select2();
        // $('#product_discount').select2();

        $('#add_data').click(function () {
            $('#productModal').modal('show');
            $('#product_form')[0].reset();
            $('#form_output').html('Yes');
            $('#button_action').val('insert');
            $('#action').val('Add');
        });

        $('#product_form').on('submit', function(event){
            event.preventDefault();
            var form_data = $(this).serialize();
            $.ajax({
                url:'{{route(\App\Http\Controllers\AppConfig::ADD_NEW_DISCOUNTED_PRODUCT_AJAX)}}',
                type:"post",
                data:form_data,
                dataType:"json",

                success:function(data)
                {
                    $('#product_form')[0].reset();
                    $('#action').val('Add');
                    $('.modal-tittle').text('Add Data');
                    $('#button_action').val('insert');
                    $('#result').html(data);
                    $('#result').addClass('alert alert-success');
                    location.reload();

                }
            })
        });

         $('#discount_form').on('submit', function(event){
                    event.preventDefault();
                    var form_data = $(this).serialize();
                    $.ajax({
                        url:'{{route(\App\Http\Controllers\AppConfig::UPDATE_DISCOUNTED_PRODUCT_AJAX)}}',
                        type:"post",
                        data:form_data,
                        dataType:"json",

                        success:function(data)
                        {
                            $('#discount_form')[0].reset();

                            location.reload();

                        }
                    })
                });



        $(document).on('click', '.delete', function(){
            var id = $(this).attr('id');
            if(confirm("Are you sure?"))
            {
                $.ajax({
                    beforeSend: function(){

                    },

                    type: 'POST',
                    url:'{{ route(\App\Http\Controllers\AppConfig::DELETE_DISCOUNTED_PRODUCT_AJAX) }}',
                    data: {
                        "_token": "{{csrf_token()}}",
                        id:id
                    },
                    cache: false,
                    dataType: 'HTML',

                    success: function(data){
                        console.log(data);
                        location.reload();
                    }
                });

            }
            else
            {
                return false;
            }
        });

        $(document).on('click', '.edit', function(){
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '{{route(\App\Http\Controllers\AppConfig::EDIT_DISCOUNTED_PRODUCT_AJAX)}}',
                data: {
                    "_token" : '{{csrf_token()}}',
                    id:id
                },
                dataType: 'json',

                success:function(data){
                    console.log(id);
                    console.log(data);

                    $('#product_discount').val(data.pro_discount);
                    $('#discountModal').modal('show');
                    $('#dis_action').val('Done');
                    $('#dis_pro_id').val(id);
                    $('.modal-title').text('Edit Data');
                    $('#dis_button_action').val('update');
                }
            });

        });



        $(document).ready(function() {
            $('#products_table').DataTable( {
                initComplete: function () {
                    this.api().columns().every( function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );

                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                }
            } );
        } );

        $(document).ready(function() {
            var table = $('#products_table').DataTable();

            $('#products_table tbody')
                .on( 'mouseenter', 'td', function () {
                    var colIdx = table.cell(this).index().column;

                    $( table.cells().nodes() ).removeClass( 'highlight' );
                    $( table.column( colIdx ).nodes() ).addClass( 'highlight' );
                } );
        } );


    </script>
@stop
