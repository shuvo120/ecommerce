
@extends('layouts.master');

<!-- header-section-ends -->
@section('top_nav')
    <div class="banner-top">
        <div class="container">
            <nav class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo">
                        <h1><a href="index.html"><span>E</span> -Shop</a></h1>
                    </div>
                </div>
                <!--/.navbar-header-->

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{route(\App\Http\Controllers\AppConfig::USER_HOME_PAGE)}}">Home</a></li>

                        {{--@foreach($categories as $category)--}}
                            {{--<li class="dropdown">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$category->cat_name}} <b class="caret"></b></a>--}}
                                {{--<ul class="dropdown-menu multi-column columns-3">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--<ul class="multi-column-dropdown">--}}
                                                {{--<h6>Subcategories</h6>--}}
                                                {{--@foreach($sub_categories as $sub_cat)--}}
                                                    {{--@if($category->id == $sub_cat->cat_id)--}}
                                                        {{--<li><a href="{{route(\App\Http\Controllers\AppConfig::GET_PRODUCT_ACCORDING_TO_SUBCATEGORY, ['name' => $sub_cat->sub_cat_name , 'id' => $sub_cat->id])}}"> {{$sub_cat->sub_cat_name}}</a></li>--}}
                                                    {{--@endif--}}
                                                {{--@endforeach--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--<ul class="multi-column-dropdown">--}}
                                                {{--<h6>CLOTHING</h6>--}}
                                                {{--<li><a href="products.html">Polos & Tees</a></li>--}}
                                                {{--<li><a href="products.html">Casual Shirts</a></li>--}}
                                                {{--<li><a href="products.html">Casual Trousers</a></li>--}}
                                                {{--<li><a href="products.html">Jeans</a></li>--}}
                                                {{--<li><a href="products.html">Shorts & 3/4th</a></li>--}}
                                                {{--<li><a href="products.html">Formal Shirts</a></li>--}}
                                                {{--<li><a href="products.html">Formal Trousers</a></li>--}}
                                                {{--<li><a href="products.html">Suits & Blazers</a></li>--}}
                                                {{--<li><a href="products.html">Track Wear</a></li>--}}
                                                {{--<li><a href="products.html">Inner Wear</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--<ul class="multi-column-dropdown">--}}
                                                {{--<h6>WATCHES</h6>--}}
                                                {{--<li><a href="products.html">Analog</a></li>--}}
                                                {{--<li><a href="products.html">Chronograph</a></li>--}}
                                                {{--<li><a href="products.html">Digital</a></li>--}}
                                                {{--<li><a href="products.html">Watch Cases</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                        {{--<div class="clearfix"></div>--}}
                                    {{--</div>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                        {{--@endforeach--}}

                        <li><a href="typography.html">TYPO</a></li>
                        <li><a href="contact.html">CONTACT</a></li>
                    </ul>
                </div>
                <!--/.navbar-collapse-->
            </nav>
            <!--/.navbar-->
        </div>
    </div>


@endsection


@section('content')
<!-- checkout -->
<div class="cart-items">
    <div class="container">
        <div class="dreamcrub">
            <ul class="breadcrumbs">
                <li class="home">
                    <a href="{{route(\App\Http\Controllers\AppConfig::USER_HOME_PAGE)}}" title="Go to Home Page">Home</a>&nbsp;
                    <span>&gt;</span>
                </li>

                <li class="women">
                    Cart
                </li>
            </ul>
            <ul class="previous">
                <li><a href="index.html">Back to Previous Page</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <h2>MY SHOPPING BAG ({{Cart::getContent()->count()}})</h2>
        <div class="cart-gd">

            @foreach($carts as $cart)
            <div class="cart-header">
                {{--<button class="khan glyphicon glyphicon-remove" id="{{$cart->id}}"> remove</button>--}}
                <div class="close1"  id="{{$cart->id}}" ></div>
                <div class="cart-sec simpleCart_shelfItem">
                    <div class="cart-item cyc">
                        <img src="{{url('storage/post_images/thumbnail/'.$cart->attributes->image)}}" class="img-responsive" alt="">
                    </div>
                    <div class="cart-item-info">
                        <h3><a href="#"> {{$cart->name}} </a><span>Pickup time:</span></h3>
                        <ul class="qty">
                            <li><p>Quantity : {{$cart->quantity}}</p></li>
                            <li><p>Min. order value:</p></li>
                            <li><p>FREE delivery</p></li>
                        </ul>
                        <div class="delivery">
                            <p>{{$cart->price}}</p>
                            <span>Delivered in 1-1:30 hours</span>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
            @endforeach
            <div>
                <p>{{Cart::getTotal()}}</p>
            </div>
        </div>
    </div>
</div>

<!-- //checkout -->
<div class="news-letter">
    <div class="container">
        <div class="join">
            <h6>JOIN OUR MAILING LIST</h6>
            <div class="sub-left-right">
                <form>
                    <input type="text" value="Enter Your Email Here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Your Email Here';}" />
                    <input type="submit" value="SUBSCRIBE" />
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="footer_top">
            <div class="span_of_4">
                <div class="col-md-3 span1_of_4">
                    <h4>Shop</h4>
                    <ul class="f_nav">
                        <li><a href="#">new arrivals</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">trends</a></li>
                        <li><a href="#">sale</a></li>
                        <li><a href="#">style videos</a></li>
                    </ul>
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>help</h4>
                    <ul class="f_nav">
                        <li><a href="#">frequently asked  questions</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                    </ul>
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>account</h4>
                    <ul class="f_nav">
                        <li><a href="account.html">login</a></li>
                        <li><a href="register.html">create an account</a></li>
                        <li><a href="#">create wishlist</a></li>
                        <li><a href="checkout.html">my shopping bag</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">create wishlist</a></li>
                    </ul>
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>popular</h4>
                    <ul class="f_nav">
                        <li><a href="#">new arrivals</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">trends</a></li>
                        <li><a href="#">sale</a></li>
                        <li><a href="#">style videos</a></li>
                        <li><a href="#">login</a></li>
                        <li><a href="#">brands</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="cards text-center">
            <img src="images/cards.jpg" alt="" />
        </div>
        <div class="copyright text-center">
            <p>© 2015 Eshop. All Rights Reserved | Design by   <a href="http://w3layouts.com">  W3layouts</a></p>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>

        // $(document).on('click', '.khan', function(){
        //     console.log('shuvo');
        //     var id = $(this).attr('id');
        //     Cart::remove(id);
        //     location.reload();
        //
        // });


        $(document).ready(function() {
            $('.close1').on('click', function(){
                var id = $(this).attr('id');
                console.log(id);
                // location.reload();
                // $('.cart-header').remove();
                $.ajax({
                    type: 'POST',
                    url: '{{route(\App\Http\Controllers\AppConfig::REMOVE_SINGLE_CART_AJAX)}}',
                    data: {
                        "_token" : '{{csrf_token()}}',
                        id:id
                    },
                    dataType: 'html',

                    success:function(data){
                        // console.log(id);
                        console.log(data);
                        location.reload();


                    }
                });
            });
        });
    </script>

    @endsection