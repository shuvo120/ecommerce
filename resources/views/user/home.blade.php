@extends('layouts.master');

@section('top_nav')
<div class="banner-top">
    <div class="container">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="logo">
                    <h1><a href="{{route(\App\Http\Controllers\AppConfig::USER_HOME_PAGE)}}"><span>E </span> -Shop</a></h1>
                </div>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="{{route(\App\Http\Controllers\AppConfig::USER_HOME_PAGE)}}">Home</a></li>

                    @foreach($categories as $category)
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$category->cat_name}} <b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="col-sm-4">
                                    <ul class="multi-column-dropdown">
                                        <h6>Subcategories</h6>
                                        @foreach($sub_categories as $sub_cat)
                                            @if($category->id == $sub_cat->cat_id)
                                                <li><a href="{{route(\App\Http\Controllers\AppConfig::GET_PRODUCT_ACCORDING_TO_SUBCATEGORY, ['name' => $sub_cat->sub_cat_name , 'id' => $sub_cat->id])}}"> {{$sub_cat->sub_cat_name}}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul class="multi-column-dropdown">
                                        <h6>CLOTHING</h6>
                                        <li><a href="products.html">Polos & Tees</a></li>
                                        <li><a href="products.html">Casual Shirts</a></li>
                                        <li><a href="products.html">Casual Trousers</a></li>
                                        <li><a href="products.html">Jeans</a></li>
                                        <li><a href="products.html">Shorts & 3/4th</a></li>
                                        <li><a href="products.html">Formal Shirts</a></li>
                                        <li><a href="products.html">Formal Trousers</a></li>
                                        <li><a href="products.html">Suits & Blazers</a></li>
                                        <li><a href="products.html">Track Wear</a></li>
                                        <li><a href="products.html">Inner Wear</a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul class="multi-column-dropdown">
                                        <h6>WATCHES</h6>
                                        <li><a href="products.html">Analog</a></li>
                                        <li><a href="products.html">Chronograph</a></li>
                                        <li><a href="products.html">Digital</a></li>
                                        <li><a href="products.html">Watch Cases</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </ul>
                    </li>
                    @endforeach

                    <li><a href="typography.html">TYPO</a></li>
                    <li><a href="contact.html">CONTACT</a></li>
                </ul>
            </div>
            <!--/.navbar-collapse-->
        </nav>
        <!--/.navbar-->
    </div>
</div>


@endsection

@section('banner')
    <div class="banner">
        <div class="container">
            <div class="banner-bottom">
                <div class="banner-bottom-left">
                    <h2>B<br>U<br>Y</h2>
                </div>
                <div class="banner-bottom-right">
                    <div  class="callbacks_container">
                        <ul class="rslides" id="slider4">
                            <li>
                                <div class="banner-info">
                                    <h3>Smart But Casual</h3>
                                    <p>Start your shopping here...</p>
                                </div>
                            </li>
                            <li>
                                <div class="banner-info">
                                    <h3>Shop Online</h3>
                                    <p>Start your shopping here...</p>
                                </div>
                            </li>
                            <li>
                                <div class="banner-info">
                                    <h3>Pack your Bag</h3>
                                    <p>Start your shopping here...</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--banner-->

                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="shop">
                <a href="{{url('/single-product/4')}}">SHOP COLLECTION NOW</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="modal fade" id="cartModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

                <form id="product_form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Insert Category Name</h4>
                    </div>


                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="content">
                            <strong>Product name: </strong>
                            <span id="proName"></span>
                            <br>
                            <br>
                            <br>
                            <strong>Product price: </strong>
                            <span id="proPrice"></span>

                            <br>
                            <br>
                            <strong>Product Quantity: </strong>
                            <select name="pro_quantity" id="pro_quantity" class="from-control input-lg">
                                <option value="" hidden> Quantity</option>

                                @for($i=1; $i <= 5; $i++)
                                    <option value="{{$i}}"> {{$i}} </option>
                                @endfor

                            </select>

                        </div>

                        <div class="modal-footer">
                            <input type="hidden" name="pro_id" id="pro_id" value="" />
                            <input type="hidden" name="button_action" id="button_action" value="insert" />
                            <input type="submit" name="submit" id="action" value="Add" class="btn btn-info" />
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </form>


            </div>

        </div>
    </div>

    <div class="container">
        <div class="main-content">
            <div class="online-strip">
                <div class="col-md-4 follow-us">
                    <h3>follow us : <a class="twitter" href="#"></a><a class="facebook" href="#"></a></h3>
                </div>
                <div class="col-md-4 shipping-grid">
                    <div class="shipping">
                        <img src="{{asset("user/images/shipping.png")}}" alt="" />
                    </div>
                    <div class="shipping-text">
                        <h3>Free Shipping</h3>
                        <p>on orders over $ 199</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 online-order">
                    <p>Order online</p>
                    <h3>Tel:999 4567 8902</h3>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="products-grid">
                <header>
                    <h3 class="head text-center">Latest Products</h3>
                </header>
                @foreach($all_products as $product)
                    <div class="col-md-4 product simpleCart_shelfItem text-center">

                        <a href="{{route(\App\Http\Controllers\AppConfig::SHOW_SINGLE_PRODUCT, ['id' =>$product->id ])}}"><img src="{{url('storage/post_images/thumbnail/'.$product->pro_img)}}"  alt="" style="width:350px;height:438px;"/></a>
                        <div class="mask">
                            <a href="{{route(\App\Http\Controllers\AppConfig::SHOW_SINGLE_PRODUCT, ['id' =>$product->id ])}}">Quick View</a>
                        </div>
                        <a class="product_name" href="{{asset("user/css/flexslider.css")}}{{route(\App\Http\Controllers\AppConfig::SHOW_SINGLE_PRODUCT, ['id' =>$product->id ])}}">{{$product->pro_name}}</a>
                        {{--<p><a class="item_add" href="#"><i></i> <span class="item_price">{{$product->pro_price}}</span></a></p>--}}
                        {{--<p><a class="cart-show" href="{{route(\App\Http\Controllers\AppConfig::ADD_CART, ['id' => $product->id])}}"><i></i> <span class="item_price">{{$product->pro_price}}</span></a></p>--}}
                        <p><a class="cart_show" id ={{$product->id}}"><i></i> <span class="item_price">{{$product->pro_price}}</span></a></p>
                    </div>
                @endforeach


                <div class="clearfix"></div>
            </div>
            <div align="center">
                {{ $all_products->links() }}
            </div>

        </div>

    </div>
    <div class="other-products">
        <div class="container">
            <h3 class="like text-center">Discounted Products</h3>
            <ul id="flexiselDemo3">
                @foreach($discounted_products as $discounted_pro)

                    <li>
                        <div class="latest-bis">
                        <a href="{{route(\App\Http\Controllers\AppConfig::SHOW_SINGLE_PRODUCT, ['id' =>$discounted_pro->id ])}}"><img src="{{url('storage/post_images/thumbnail/'.$discounted_pro->pro_img)}}"  class="dis-img img-responsive" alt="" /></a>
                        <div class="product liked-product simpleCart_shelfItem">
                            <a class="like_name" href="{{route(\App\Http\Controllers\AppConfig::SHOW_SINGLE_PRODUCT, ['id' =>$discounted_pro->id ])}}</a>
                            <p><a class="item_add" href="#"><i></i> <span class="item_price">${{ floor(($discounted_pro->pro_price) - (($discounted_pro->pro_discount * $discounted_pro->pro_price)/(100))) }}</span></a></p>

                        </div>
                        <div class="offer">
                        <p>{{$discounted_pro->pro_discount}}%</p>
                        {{--<small>Top Offer</small>--}}
                        </div>
                        </div>

                        {{--<div>--}}

                            {{--<h3> <del> <span class=" item_price">${{$discounted_pro->pro_price}}</span></del> ( -{{$discounted_pro->pro_discount}}% )</h3>--}}
                        {{--</div>--}}
                    </li>
                    @endforeach
            </ul>

        </div>
    </div>
    <!-- content-section-ends-here -->
    <div class="news-letter">
        <div class="container">
            <div class="join">
                <h6>JOIN OUR MAILING LIST</h6>
                <div class="sub-left-right">
                    <form>
                        <input type="text" value="Enter Your Email Here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Your Email Here';}" />
                        <input type="submit" value="SUBSCRIBE" />
                    </form>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="footer_top">
                <div class="span_of_4">
                    <div class="col-md-3 span1_of_4">
                        <h4>Shop</h4>
                        <ul class="f_nav">
                            <li><a href="#">new arrivals</a></li>
                            <li><a href="#">men</a></li>
                            <li><a href="#">women</a></li>
                            <li><a href="#">accessories</a></li>
                            <li><a href="#">kids</a></li>
                            <li><a href="#">brands</a></li>
                            <li><a href="#">trends</a></li>
                            <li><a href="#">sale</a></li>
                            <li><a href="#">style videos</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 span1_of_4">
                        <h4>help</h4>
                        <ul class="f_nav">
                            <li><a href="#">frequently asked  questions</a></li>
                            <li><a href="#">men</a></li>
                            <li><a href="#">women</a></li>
                            <li><a href="#">accessories</a></li>
                            <li><a href="#">kids</a></li>
                            <li><a href="#">brands</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 span1_of_4">
                        <h4>account</h4>
                        <ul class="f_nav">
                            <li><a href="{{route(\App\Http\Controllers\AppConfig::USER_LOGIN)}}">login</a></li>
                            <li><a href="{{route(\App\Http\Controllers\AppConfig::USER_REGISTRATION)}}">create an account</a></li>
                            <li><a href="#">create wishlist</a></li>
                            <li><a href="checkout.html">my shopping bag</a></li>
                            <li><a href="#">brands</a></li>
                            <li><a href="#">create wishlist</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 span1_of_4">
                        <h4>popular</h4>
                        <ul class="f_nav">
                            <li><a href="#">new arrivals</a></li>
                            <li><a href="#">men</a></li>
                            <li><a href="#">women</a></li>
                            <li><a href="#">accessories</a></li>
                            <li><a href="#">kids</a></li>
                            <li><a href="#">brands</a></li>
                            <li><a href="#">trends</a></li>
                            <li><a href="#">sale</a></li>
                            <li><a href="#">style videos</a></li>
                            <li><a href="#">login</a></li>
                            <li><a href="#">brands</a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="cards text-center">
                <img src="{{asset("user/images/cards.jpg")}}" alt="" />
            </div>
            <div class="copyright text-center">
                <p>Hi | Customized by  <a href="http://w3layouts.com">  Shuvo</a></p>
            </div>
        </div>
    </div>
    @endsection

@section('js')
    <script type="text/javascript">
        $(window).load(function() {
            $("#flexiselDemo3").flexisel({
                visibleItems: 4,
                animationSpeed: 1000,
                autoPlay: true,
                autoPlaySpeed: 3000,
                pauseOnHover: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: {
                    portrait: {
                        changePoint:480,
                        visibleItems: 1
                    },
                    landscape: {
                        changePoint:640,
                        visibleItems: 2
                    },
                    tablet: {
                        changePoint:768,
                        visibleItems: 3
                    }
                }
            });

        });
    </script>
    <script type="text/javascript" src="{{asset("user/js/jquery.flexisel.js")}}"></script>
    <script>
        $('.dis-img').croppie();
    </script>

    <script src="{{asset("user/js/responsiveslides.min.js")}}"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider4").responsiveSlides({
                auto: true,
                pager:true,
                nav:false,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });

        $(document).on('click', '.cart_show', function () {

            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '{{route(\App\Http\Controllers\AppConfig::GET_SINGLE_PRODUCT_AJAX)}}',
                data: {
                    "_token": '{{csrf_token()}}',
                    id:id
                },

                dataType: 'json',

                success:function(data) {
                    console.log(data);
                    console.log(data['pro_name']);

                    $('#proName').html(data.pro_name);
                    $('#proPrice').html(data.pro_price);
                    $('#pro_id').val(id);
                    $('#cartModal').modal('show');
                }

            });
        });

        $('#product_form').on('submit', function(event){
            event.preventDefault();
            var form_data = $(this).serialize();
            $.ajax({
                url:'{{route(\App\Http\Controllers\AppConfig::ADD_CART)}}',
                type:"post",
                data:form_data,
                dataType:"HTML",

                success:function(data)
                {
                    console.log(data);
                    $('#product_form')[0].reset();
                    location.reload();

                }
            })
        });

        {{--$(document).on('click', '.', function(){--}}
            {{--var id = $(this).attr('id');--}}
            {{--$.ajax({--}}
                {{--type: 'POST',--}}
                {{--url: '{{route(\App\Http\Controllers\AppConfig::GET_SINGLE_PRODUCT_AJAX)}}',--}}
                {{--data: {--}}
                    {{--"_token" : '{{csrf_token()}}',--}}
                    {{--id:id--}}
                {{--},--}}
                {{--dataType: 'json',--}}

                {{--success:function(data){--}}
                    {{--console.log(id);--}}
                    {{--console.log(data);--}}

                    {{--// $('#cat_name').val(data.cat_name);--}}
                    {{--// $('#catModal').modal('show');--}}
                    {{--// $('#action').val('Edit');--}}
                    {{--// $('#cat_id').val(id);--}}
                    {{--// $('.modal-title').text('Edit Data');--}}
                    {{--// $('#button_action').val('update');--}}
                {{--}--}}
            {{--});--}}

        {{--});--}}
    </script>

@endsection