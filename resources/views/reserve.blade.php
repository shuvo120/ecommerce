public static function getAllParkingInfoByCounty($countyId) {

return self::select('parking_location.id','parking_location.address','parking_location.parking_short','parking_location.parking_name','primary_tbl_city.city_name','primary_tbl_zip_code.zip_code')
->where('parking_location.county_id',$countyId)
->join('primary_tbl_city', 'primary_tbl_city.id','=','parking_location.city_id')
->join('primary_tbl_zip_code', 'primary_tbl_zip_code.id','=','parking_location.zip_id')
->orderBy('primary_tbl_city.city_name')
->get();
}