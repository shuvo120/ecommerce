
<!DOCTYPE html>
<html>
<head>
    <title>Eshop a Flat E-Commerce Bootstrap Responsive Website Template | Home :: w3layouts</title>
    <link href="{{asset("user/css/bootstrap.css")}}" rel='stylesheet' type='text/css' />
{{--    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">--}}
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset("user/js/jquery.min.js")}}"></script>
    <!-- Custom Theme files -->
    <link href="{{asset("user/css/style.css")}}" rel="stylesheet" type="text/css" media="all" />
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Eshop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfont-->
    <!-- for bootstrap working -->
    <script type="text/javascript" src="{{asset("user/js/bootstrap-3.1.1.min.js")}}"></script>
    <!-- //for bootstrap working -->
    <!-- cart -->
{{--    <script src="{{asset("user/js/simpleCart.min.js")}}"> </script>--}}
    <!-- cart -->
    <link rel="stylesheet" href="{{asset("user/css/flexslider.css")}}" type="text/css" media="screen" />

    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.min.css') }}">


</head>


<body>
<!-- header-section-starts -->
<div class="header">
    <div class="header-top-strip">
        <div class="container">
            <div class="header-top-left">
                @if(\Illuminate\Support\Facades\Session::has('user_id'))
{{--                    {{$session_id}}--}}
{{--                    {{\Illuminate\Support\Facades\Session::get('user_id')}}--}}

                    {{--<li class="dropdown user user-menu open">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">--}}
                            {{--<img src="{{asset('images\profile_images\me.jpg')}}" class="user-image img-circle"  style="height:33px;width:33px" alt="User Image">--}}
                            {{--<span class="hidden-xs"> Hi, {{\Illuminate\Support\Facades\Session::get('user_name')}}</span>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<!-- User image -->--}}
                            {{--<li class="user-header">--}}
                                {{--<img src="{{asset('images\profile_images\me.jpg')}}" class="img-circle" alt="User Image" width="111" height="111">--}}

                                {{--<p>--}}
                                    {{--Alexander Pierce - Web Developer--}}
                                    {{--<small>Member since Nov. 2012</small>--}}
                                {{--</p>--}}
                            {{--</li>--}}
                            {{--<!-- Menu Body -->--}}
                            {{--<li class="user-body">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-xs-4 text-center">--}}
                                        {{--<a href="#">Followers</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-xs-4 text-center">--}}
                                        {{--<a href="#">Sales</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-xs-4 text-center">--}}
                                        {{--<a href="#">Friends</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<!-- /.row -->--}}
                            {{--</li>--}}
                            {{--<!-- Menu Footer-->--}}
                            {{--<li class="">--}}
                                {{--<div class="pull-left">--}}
                                    {{--<a href="#" class="btn  btn-flat">Profile</a>--}}
                                {{--</div>--}}
                                {{--<div class="pull-right">--}}
                                    {{--<a href="#" class="btn btn-default btn-flat">Sign out</a>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}

                    <div class="navbar-custom-menu">
                        <div class="dropdown admin-img">
                            <a class=" dropdown-toggle" type="button" data-toggle="dropdown">
                                <img src="{{asset('images\profile_images\me.jpg')}}" class="img-circle" style="height:33px;width:33px" alt="Avatar">

                                <span class="caret"></span> <span class="hidden-xs" style="color:white;"> Hi, {{\Illuminate\Support\Facades\Session::get('user_name')}} !</span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <div class="text-center">
                                        <div class="">

                                            <img src="{{asset('images\profile_images\me.jpg')}}" class="img-circle" alt="Cinque Terre" width="111" height="111">


                                        </div>
                                        <h3 class="font-italic">{{\Illuminate\Support\Facades\Session::get('user_name')}}</h3>

                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li style="color:blue;" >
                                    @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                        <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                            <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                        </a>
                                    @else
                                        <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                        </a>
                                        <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                            @if(config('adminlte.logout_method'))
                                                {{ method_field(config('adminlte.logout_method')) }}
                                            @endif
                                            {{ csrf_field() }}
                                        </form>
                                    @endif
                                </li>

                                <li>
                                    <a href="{{route(\App\Http\Controllers\AppConfig::SETTINGS)}}" class="glyphicon glyphicon-pencil">
                                        Settings
                                    </a>
                                </li>

                                <li>
                                    <a href="{{route(\App\Http\Controllers\AppConfig::SETTINGS)}}" class="glyphicon glyphicon-lock">
                                        Change password
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="{{route(\App\Http\Controllers\AppConfig::ABOUT_US)}}"> About Us</a></li>
                            </ul>
                        </div>

                    </div>
                @else
                <ul>
                    <li><a href="{{route(\App\Http\Controllers\AppConfig::USER_LOGIN)}}"><span class="glyphicon glyphicon-user"> </span>Login</a></li>
                    <li><a href="{{route(\App\Http\Controllers\AppConfig::USER_REGISTRATION)}}"><span class="glyphicon glyphicon-lock"> </span>Create an Account</a></li>
                </ul>
                @endif
            </div>
            <div class="header-right">
                <div class="cart box_1">

                    <a href="{{route(\App\Http\Controllers\AppConfig::CART_CHECKOUT)}}">
                        <?php

//                        $userId = auth()->user()->id;

//                        $cartCollection = Cart::session($userId)->getTotalQuantity();
                        $cartCollection = Cart::getTotalQuantity();
                        ?>
                        {{--<h3> <span class="simpleCart_total"> $0.00 </span> (<span id="simpleCart_quantity" class="simpleCart_quantity"> 0 </span>)<img src="images/bag.png" alt=""></h3>--}}
                        @if($cartCollection == 0)
                                <h3><span>Empty cart</span></h3>


                        @elseif($cartCollection == 1)
                            <h3> <span class="glyphicon glyphicon-shopping-cart"></span> {{Cart::getTotal()}}$ ({{$cartCollection}})</h3>
                        @else
                            <h3> <span class="glyphicon glyphicon-shopping-cart"></span> {{Cart::getTotal()}}$ ({{$cartCollection}})</h3>
                        @endif
                    </a>

                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- header-section-ends -->
<<div>
    @yield('top_nav')

</div>

<div>
    @yield('banner')

</div>

<div>
    @yield('inner_banner')
</div>

<!-- content-section-starts-here -->
<div>
    @yield('content')
</div>

<div>
    @yield('js')
</div>
</body>
</html>