<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AppConfig;

//Route::get('/', function () {
//    return view('user/home');
//});

//Auth::routes();
Auth::routes(['verify' => true]);

Route::get('/admin', 'HomeController@index')->name('admin');


Route::get('/show-cart', 'CartController@index');

//Route::get('/single-product', 'HomeController@viewSingleProduct')->name('single.product');


Route::post('/add-cart',                           ['as' => AppConfig::ADD_CART,                                        'uses' => 'CartController@addCart']);
Route::get('/showw-cart',                          ['as' => AppConfig::CART_CHECKOUT,                                   'uses' => 'CartController@checkCart']);
Route::post('/single-product',                     ['as' => AppConfig::GET_SINGLE_PRODUCT_AJAX,                         'uses' => 'CartController@getSingleProduct']);
Route::post('/remove-product',                     ['as' => AppConfig::REMOVE_SINGLE_CART_AJAX,                         'uses' => 'CartController@removeSingleProduct']);




Route::get('/',                                    ['as' => AppConfig::BLANK,                                           'uses' => 'HomeController@userHome']);
Route::get('/home',                                ['as' => AppConfig::USER_HOME_PAGE,                                  'uses' => 'HomeController@userHome']);

Route::get('/single-product/{id}',                 ['as' => AppConfig::SHOW_SINGLE_PRODUCT,                             'uses' => 'HomeController@viewSingleProduct']);
Route::get('/login-user',                          ['as' => AppConfig::USER_LOGIN,                                      'uses' => 'AdminController@viewLogin']);
Route::get('/register-user',                       ['as' => AppConfig::USER_REGISTRATION,                               'uses' => 'AdminController@viewRegistration']);



Route::get('/getAllCat',                           ['as' => AppConfig::SHOW_ALL_CATEGORY,                               'uses' => 'CategoriesController@getAllCat']);
Route::post('/add-category',                       ['as' => AppConfig::ADD_CATEGORY,                                    'uses' => 'CategoriesController@addCat']);
Route::post('/edit-category',                      ['as' => AppConfig::EDIT_CATEGORY_AJAX,                              'uses' => 'CategoriesController@editCat']);
Route::post('/delete-category',                    ['as' => AppConfig::ROUTE_DELETE_CATEGORY_AJAX,                      'uses' => 'CategoriesController@deleteCat']);


Route::get('/subCat/{id}',                         ['as' => AppConfig::BLANK,                                           'uses' => 'SubCategoriesController@index']);

Route::post('/add-sub-category',                   ['as' => AppConfig::ADD_SUB_CATEGORY,                                'uses' => 'SubCategoriesController@addCat']);
Route::post('/edit-sub-category',                  ['as' => AppConfig::EDIT_SUB_CATEGORY_AJAX,                          'uses' => 'SubCategoriesController@editCat']);
Route::post('/delete-sub-category',                ['as' => AppConfig::DELETE_SUB_CATEGORY_AJAX,                        'uses' => 'SubCategoriesController@deleteCat']);

Route::GET('/add-product',                         ['as' => AppConfig::BLANK,                                           'uses' => 'ProductController@index']);
Route::GET('/show-products',                       ['as' => AppConfig::BLANK,                                           'uses' => 'ProductController@showProducts']);


Route::post('/get-sub-cat',                        ['as' => AppConfig::SUBCATEGORY_ACCORDING_TO_CATEGORY_AJAX,          'uses' => 'ProductController@getSubCat']);
Route::post('/add-product',                        ['as' => AppConfig::ADD_NEW_PRODUCT,                                 'uses' => 'ProductController@uploadTest']);
Route::post('/add-product-by-ajax',                ['as' => AppConfig::ADD_NEW_PRODUCT_AJAX,                            'uses' => 'ProductController@uploadPost']);
Route::post('/edit-product',                       ['as' => AppConfig::EDIT_PRODUCT_AJAX,                               'uses' => 'ProductController@editProduct']);
Route::post('/delete-product',                     ['as' => AppConfig::DELETE_PRODUCT_AJAX,                             'uses' => 'ProductController@deleteProduct']);
Route::post('/update-product',                     ['as' => AppConfig::UPDATE_PRODUCT_AJAX,                             'uses' => 'ProductController@updateProduct']);
Route::get('/{name}/{id}',                         ['as' => AppConfig::GET_PRODUCT_ACCORDING_TO_SUBCATEGORY,            'uses' => 'ProductController@getProductAccordingToSubcat']);



Route::post('/review',                              ['as' => AppConfig::ADD_A_REVIEW,                                    'uses' => 'ProductsReviewController@addReview']);



Route::get('/settings-',                           ['as'=>AppConfig::SETTINGS,                                          'uses'=> 'AdminController@index']);
Route::get('/about-us',                            ['as'=>AppConfig::ABOUT_US,                                          'uses' =>'AdminController@aboutUs']);

Route::get('/discounts',                           ['as'=>AppConfig::SHOW_ALL_DISCOUNTED_PRODUCTS,                      'uses' =>'DiscountController@index']);
Route::post('/discount-product',                   ['as'=>AppConfig::ADD_NEW_DISCOUNTED_PRODUCT_AJAX,                   'uses' =>'DiscountController@addProduct']);
Route::post('/edit-discount-product',              ['as'=>AppConfig::EDIT_DISCOUNTED_PRODUCT_AJAX,                      'uses' =>'DiscountController@editProduct']);
Route::post('/update-discount-product',            ['as'=>AppConfig::UPDATE_DISCOUNTED_PRODUCT_AJAX,                    'uses' =>'DiscountController@updateProduct']);
Route::post('/delete-discount-product',            ['as'=>AppConfig::DELETE_DISCOUNTED_PRODUCT_AJAX,                    'uses' =>'DiscountController@deleteProduct']);



Route::get('/delete_stu',                          ['as'=>AppConfig::ROUTE_DELETE ,                                     'uses'=> 'StudentsController@deleteStu']);
